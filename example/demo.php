<?php declare(strict_types=1);

use Librarian\Lendings\BookLendingProcess;
use Librarian\Lendings\Command\AddNeed;
use Librarian\Lendings\Command\CancelNeed;
use Librarian\Lendings\Command\Handler\AddNeedHandler;
use Librarian\Lendings\Command\Handler\CancelNeedHandler;
use Librarian\Lendings\Command\Ping;
use Librarian\Lendings\Event\NeedAdded;
use Librarian\Lendings\Event\NeedExpired;
use Librarian\Lendings\Infrastructure\Repository\InMemoryReaderNeedsList;
use Librarian\Lendings\ReaderNeedsList;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\Plugin\Router\CommandRouter;
use Prooph\ServiceBus\Plugin\Router\EventRouter;

require_once __DIR__ . '/../vendor/autoload.php';

$commandBus = new CommandBus();
$lendingProcess = new BookLendingProcess($commandBus);

$eventBus = new EventBus();
$eventRouter = new EventRouter();

$eventRouter->attachToMessageBus($eventBus);

$eventRouter
    ->route(NeedAdded::class)
    ->to(function (NeedAdded $needAdded) {
        var_dump($needAdded);
    });

$processEvents = [
    NeedAdded::class,
    NeedExpired::class,
];

foreach ($processEvents as $event) {
    $eventRouter
        ->route($event)
        ->to($lendingProcess);
}

$lists = new InMemoryReaderNeedsList($eventBus);


$commandRouter = new CommandRouter();

$commandRouter->attachToMessageBus($commandBus);
$commandRouter
    ->route(AddNeed::class)
    ->to(new AddNeedHandler($lists));
$commandRouter
    ->route(CancelNeed::class)
    ->to(new CancelNeedHandler($lists));
$commandRouter
    ->route(Ping::class)
    ->to(function (Ping $cmd) {
        var_dump('PING!');
    });

$commandBus->dispatch(new AddNeed(1, 1));

// 2 tygodnie oczekiwania...
$eventBus->dispatch(new NeedExpired(1, 1));

$list = $lists->find(1);
var_dump($list);

exit;
//$commandBus->dispatch(new AddNeed(1, 2));
//$commandBus->dispatch(new AddNeed(1, 3));
//$commandBus->dispatch(new AddNeed(1, 4));
//$commandBus->dispatch(new CancelNeed(1, 2));
//$commandBus->dispatch(new AddNeed(1, 2));
//$commandBus->dispatch(new CancelNeed(1, 2));

//var_dump($lists->find(1));
$list = $lists->find(1);

//var_dump($fromEventList);