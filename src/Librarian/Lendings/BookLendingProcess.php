<?php declare(strict_types=1);

namespace Librarian\Lendings;

use Librarian\Lendings\Command\CancelNeed;
use Librarian\Lendings\Command\Ping;
use Librarian\Lendings\Event\Event;
use Librarian\Lendings\Event\NeedAdded;
use Librarian\Lendings\Event\NeedCancelled;
use Librarian\Lendings\Event\NeedExpired;
use Prooph\ServiceBus\CommandBus;

class BookLendingProcess
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * BookLendingProcess constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Event $event)
    {
        switch (get_class($event)) {
            case NeedAdded::class:
                // zapis na listę per medium
                $this->commandBus->dispatch(new Ping());
                break;
            case NeedCancelled::class:
                // usuniecie z listy per medium
                $this->commandBus->dispatch(new Ping());
                break;
            case NeedExpired::class:
                $this->commandBus->dispatch(
                    new CancelNeed(
                        $event->getReaderId(),
                        $event->getBookId()
                    )
                );
                $this->commandBus->dispatch(
                    new NotifyReader(
                        $event->getReaderId(),
                        'EXPIRED!'
                    )
                );
                break;
        }
    }
}