<?php declare(strict_types=1);

namespace Librarian\Lendings\Command;

class AddNeed
{
    /**
     * @var int
     */
    private $readerId;

    /**
     * @var int
     */
    private $bookId;

    /**
     * AddNeed constructor.
     * @param int $readerId
     * @param int $bookId
     */
    public function __construct(int $readerId, int $bookId)
    {
        $this->readerId = $readerId;
        $this->bookId = $bookId;
    }

    /**
     * @return int
     */
    public function getReaderId(): int
    {
        return $this->readerId;
    }

    /**
     * @return int
     */
    public function getBookId(): int
    {
        return $this->bookId;
    }
}