<?php declare(strict_types=1);

namespace Librarian\Lendings\Command\Handler;

use Librarian\Lendings\Command\AddNeed;
use Librarian\Lendings\ReaderNeedsList;
use Librarian\Lendings\ReaderNeedsListRepository;

class AddNeedHandler
{
    /**
     * @var ReaderNeedsListRepository
     */
    private $lists;

    /**
     * AddNeedHandler constructor.
     * @param ReaderNeedsListRepository $lists
     */
    public function __construct(ReaderNeedsListRepository $lists)
    {
        $this->lists = $lists;
    }

    public function __invoke(AddNeed $command)
    {
        $list = $this->lists->find($command->getReaderId());

        if (!$list instanceof ReaderNeedsList) {
            $list = new ReaderNeedsList($command->getReaderId());
        }

        $list->addNeed($command);

        $this->lists->save($list);
    }
}
