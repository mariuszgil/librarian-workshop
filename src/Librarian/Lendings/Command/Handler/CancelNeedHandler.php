<?php declare(strict_types=1);

namespace Librarian\Lendings\Command\Handler;

use Librarian\Lendings\Command\CancelNeed;
use Librarian\Lendings\ReaderNeedsListRepository;

class CancelNeedHandler
{
    /**
     * @var ReaderNeedsListRepository
     */
    private $lists;

    /**
     * CancelNeedHandler constructor.
     * @param ReaderNeedsListRepository $lists
     */
    public function __construct(ReaderNeedsListRepository $lists)
    {
        $this->lists = $lists;
    }

    public function __invoke(CancelNeed $command)
    {
        $list = $this->lists->find($command->getReaderId());

        $list->cancelNeed($command->getBookId());

        $this->lists->save($list);
    }
}
