<?php declare(strict_types=1);

namespace Librarian\Lendings\Command\Handler;

use Librarian\Lendings\Command\CancelNeed;
use Librarian\Lendings\ReaderNeedsListRepository;

class CommandHandler
{
    /**
     * @var
     */
    private $repo;

    /**
     * CommandHandler constructor.
     * @param $repo
     */
    public function __construct($repo)
    {
        $this->repo = $repo;
    }

    public function __invoke(Command $command)
    {
        $object = $this->repo->find($command->getId());

        $object->handle($command);

        $this->repo->save($object);
    }
}
