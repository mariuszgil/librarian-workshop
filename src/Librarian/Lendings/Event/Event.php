<?php declare(strict_types=1);

namespace Librarian\Lendings\Event;

abstract class Event
{
    /**
     * @var \DateTimeImmutable
     */
    private $created;

    /**
     * Event constructor.
     */
    public function __construct()
    {
        $this->created = new \DateTimeImmutable();
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreated(): \DateTimeImmutable
    {
        return $this->created;
    }
}