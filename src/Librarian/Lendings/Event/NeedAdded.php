<?php declare(strict_types=1);

namespace Librarian\Lendings\Event;

class NeedAdded extends Event
{
    /**
     * @var int
     */
    private $readerId;

    /**
     * @var int
     */
    private $bookId;

    /**
     * NeedAdded constructor.
     * @param int $readerId
     * @param int $bookId
     */
    public function __construct(int $readerId, int $bookId)
    {
        parent::__construct();

        $this->readerId = $readerId;
        $this->bookId = $bookId;
    }

    /**
     * @return int
     */
    public function getReaderId(): int
    {
        return $this->readerId;
    }

    /**
     * @return int
     */
    public function getBookId(): int
    {
        return $this->bookId;
    }
}
