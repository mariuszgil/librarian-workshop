<?php declare(strict_types=1);

namespace Librarian\Lendings\Infrastructure\Repository;

use Librarian\Lendings\ReaderNeedsList;
use Librarian\Lendings\ReaderNeedsListRepository;
use Prooph\ServiceBus\EventBus;

class InMemoryReaderNeedsList implements ReaderNeedsListRepository
{
    /**
     * @var array
     */
    private $lists = [];

    /**
     * @var EventBus
     */
    private $eventBus;

    /**
     * InMemoryReaderNeedsList constructor.
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function save(ReaderNeedsList $readerNeedsList): void
    {
        $this->lists[$readerNeedsList->getReaderId()] = $readerNeedsList;

        // @todo - chałupka solution ;)
        foreach ($readerNeedsList->fetchEvents() as $event) {
            $this->eventBus->dispatch($event);
        }
    }

    public function find(int $id): ?ReaderNeedsList
    {
        // @todo ;)

        return $this->lists[$id] ?? null; // ?? throw new InvalidArgumentException();
    }
}
