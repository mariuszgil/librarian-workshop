<?php declare(strict_types=1);

namespace Librarian\Lendings;

use Librarian\Lendings\Command\AddNeed;
use Librarian\Lendings\Event\Event;
use Librarian\Lendings\Event\NeedAdded;
use Librarian\Lendings\Event\NeedCancelled;

class ReaderNeedsList
{
    /**
     * @var int
     */
    private $readerId;

    /**
     * @var array
     */
    private $needs = [];

    /**
     * @var int
     */
    private $maxSize = 5;

    /**
     * @var Event[]
     */
    private $events = [];

    /**
     * @var int
     */
    private $version = 1;

    private $cancellations = 0;

    /**
     * ReaderNeedsList constructor.
     * @param int $readerId
     */
    public function __construct(int $readerId)
    {
        $this->readerId = $readerId;
    }

    public static function fromEvents(int $readerId, array $events): self
    {
        $list = new self($readerId);

        foreach ($events as $event) {
            $list->apply($event);
        }

        return $list;
    }

    // Java style...
    //public function handle(AddNeed $command);
    //public function handle(CancelNeed $command);

    public function addNeed(AddNeed $command): void
    {
        if ($this->readerId !== $command->getReaderId()) {
            throw new \LogicException("Another user list");
        }

        if (count($this->needs) >= $this->maxSize) {
            throw new \LogicException("List is full");
        }

        if (in_array($command->getBookId(), $this->needs)) {
            throw new \LogicException("Duplicated need on list");
        }

        $this->record(new NeedAdded($this->readerId, $command->getBookId()));
    }

    public function cancelNeed(int $bookId): void
    {
        if (!in_array($bookId, $this->needs)) {
            throw new \LogicException("Cant remove nonexisting book from list");
        }

        $this->record(new NeedCancelled($this->readerId, $bookId));
    }

    private function record(Event $event): void
    {
        $this->events[]= $event;

        $this->apply($event);
    }

    private function apply(Event $event): void
    {
        switch (get_class($event)) {
            case NeedAdded::class:
                $this->needs[] = $event->getBookId();
                break;
            case NeedCancelled::class:
                $key = array_keys($this->needs, $event->getBookId())[0];
                unset($this->needs[$key]);
                $this->cancellations++;
                break;
        }

        $this->version++;
    }

    /**
     * @return int
     */
    public function getReaderId(): int
    {
        return $this->readerId;
    }

    /**
     * @return array
     */
    public function getNeeds(): array
    {
        return $this->needs;
    }

    /**
     * @return int
     */
    public function getMaxSize(): int
    {
        return $this->maxSize;
    }

    // @todo usunać...

    /**
     * @return Event[]
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @todo chałupka solution ;)
     *
     * @return array
     */
    public function fetchEvents(): array
    {
        $events = $this->getEvents();

        $this->events = [];

        return $events;
    }
}