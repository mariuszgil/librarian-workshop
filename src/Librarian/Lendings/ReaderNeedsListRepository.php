<?php declare(strict_types=1);

namespace Librarian\Lendings;

interface ReaderNeedsListRepository
{
    public function save(ReaderNeedsList $readerNeedsList): void;

    public function find(int $id): ?ReaderNeedsList;
}